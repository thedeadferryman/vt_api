lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vt_api/version'

Gem::Specification.new do |spec|
	spec.name    = 'vt_api'
	spec.version = VtApi::VERSION
	spec.authors = [%{Karl 'Charon' Meinkopf}]
	spec.email   = ['kremen.karl@yandex.com']

	spec.summary     = %{VirusTotal API bindings for Ruby}
	spec.description = %{}
	spec.homepage    = 'http://nuklear.bitbucket.io/vt_api/'
	spec.license     = 'Apache-2.0'

	if spec.respond_to?(:metadata)
		spec.metadata['allowed_push_host'] = 'https://rubygems.org/'

		spec.metadata['homepage_uri']    = spec.homepage
		spec.metadata['source_code_uri'] = 'https://bitbucket.org/thedeadferryman/vt_api/'
		spec.metadata['changelog_uri']   = 'https://bitbucket.org/thedeadferryman/vt_api/'
	else
		raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
	end

	spec.files = []

	Dir.chdir(File.expand_path('..', __FILE__)) do
		spec.files += `git ls-files -z`.split(0.chr).reject { |f| f.match(%r{^(test|spec|features)/}) }
	end


	spec.bindir        = 'exe'
	spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
	spec.require_paths = ['lib']

	spec.add_development_dependency 'bundler', '~> 2.0'
	spec.add_development_dependency 'rake', '~> 10.0'
	spec.add_development_dependency 'rspec', '~> 3.8'
	spec.add_development_dependency 'sinatra', '~> 2.0'
	spec.add_development_dependency 'webmock', '~> 3.5'
	spec.add_development_dependency 'yard', '~> 0.9'
	# spec.add_development_dependency 'pry', '~> 0.12'

	spec.add_dependency 'faraday', '~> 0.15'
	spec.add_dependency 'mimemagic', '~> 0.3'
end
