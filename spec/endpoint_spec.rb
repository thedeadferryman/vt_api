require 'rspec'

require_relative '../lib/vt_api'


describe 'Internal::Endpoint, created correctly' do

	let(:endpoint) { VtApi::Endpoint.new 'foo/bar', :get, { foo: true, bar: false, baz: true } }
	let(:only_required_params) { { foo: 'abc', baz: 'boom' } }
	let(:all_params) { { foo: 'abc', baz: 'boom', bar: 'pew' } }
	let(:missing_required_params) { { baz: 'boom', bar: 'pew' } }

	context 'when only required params passed' do
		it '#params_valid? should be true' do
			expect(endpoint.params_valid?(only_required_params)).to eql(true)
		end

		it 'missing_params should be empty' do
			expect(endpoint.missing_params(only_required_params)).to be_empty
		end
	end

	context 'when all params passed' do
		it '#params_valid? should be true' do
			expect(endpoint.params_valid?(all_params)).to eql(true)
		end

		it 'missing_params should be empty' do
			expect(endpoint.missing_params(all_params)).to be_empty
		end
	end

	context 'when required param missed' do
		it '#params_valid? should be false' do
			expect(endpoint.params_valid?(missing_required_params)).to eql(false)
		end

		it 'missing_params should contain :foo' do
			expect(endpoint.missing_params(missing_required_params)).not_to be_empty
			# noinspection RubyResolve
			expect(endpoint.missing_params(missing_required_params)).to include(:foo)
		end
	end
end