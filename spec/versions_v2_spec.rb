require 'rspec'
require 'rspec/expectations'

require_relative '../lib/vt_api'

RSpec::Matchers.define :be_url do |expected|
	# The match method, returns true if valie, false if not.
	match do |actual|
		# Use the URI library to parse the string, returning false if this fails.
		URI.parse(actual) rescue false
	end
end

describe 'Versions::ApiV2, singleton' do

	let(:api) {VtApi::Versions::DEFAULT}
	let(:existing_method) {'file.report'}
	let(:missing_method) {'file.file'}
	let(:existing_error) {403}
	let(:missing_error) {301}

	context 'when existing method requested' do
		it '#endpoint? should be true' do
			expect(api.endpoint?(existing_method)).to eql(true)
		end

		it '#endpoint should return Endpoint' do
			expect(api.endpoint(existing_method)).to be_kind_of VtApi::Endpoint
		end
	end

	context 'when nonexistent method requested' do
		it '#endpoint? should be false' do
			expect(api.endpoint?(missing_method)).to eql(false)
		end

		it '#endpoint should return nil' do
			expect(api.endpoint(missing_method)).to be_nil
		end
	end

	context 'when existing error requested' do
		it '#error? should be true' do
			expect(api.error?(existing_error)).to eql(true)
		end

		it '#error should return String' do
			expect(api.error(existing_error)).to be_kind_of String
		end
	end

	context 'when nonexistent error requested' do
		it '#error? should be false' do
			expect(api.error?(missing_error)).to eql(false)
		end

		it '#error should return nil' do
			expect(api.error(missing_error)).to be_nil
		end
	end

	context 'when const methods called' do
		it '#version should return String' do
			expect(api.version).to be_kind_of String
		end

		it '#base_url should return valid URL' do
			expect(api.base_url.to_s).to be_url
		end
	end

end