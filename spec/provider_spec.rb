# frozen_string_literal: true

require 'rspec'
require 'json'
require 'webmock/rspec'

require_relative '../lib/vt_api'
require_relative 'support/fake_api2'

describe 'API Provider for VT Public API 2.0' do

	before do
		stub_request(:any, Regexp.compile(VtApi::Versions::API_V2.base_url)).to_rack FakeApi2
	end

	let(:provider) { VtApi::ApiV2.provider }

	let(:valid_method) { 'file.report' }
	let(:invalid_method) { 'foo.bar' }

	let(:valid_params) { { apikey: APIKEY, resource: FINISHED_RESOURCE } }
	let(:missed_params) { { apikey: APIKEY } }
	let(:no_apikey_params) { { resource: FINISHED_RESOURCE } }
	let(:invalid_apikey_params) { { apikey: 'dummy', resource: FINISHED_RESOURCE } }


	context 'Valid method with valid params' do
		it '#request should return with response_code 1' do
			result = provider.request(valid_method, valid_params)

			expect(result.response_code).to eql 1
		end
	end

	context 'Valid method with invalid params' do
		it '#request should throw MissingParametersError (missing parameter)' do
			expect { provider.request(valid_method, missed_params) }
				.to raise_error VtApi::MissingParametersError
		end

		it '#request should throw MissingParametersError (no apikey)' do
			expect { provider.request(valid_method, no_apikey_params) }
				.to raise_error VtApi::MissingParametersError
		end

		it '#request should throw ApiError (invalid apikey)' do
			expect { provider.request(valid_method, invalid_apikey_params) }
				.to raise_error(VtApi::ApiError)
		end
	end

	context 'Invalid method' do
		it '#request should throw UndefinedMethodError' do
			expect { provider.request(invalid_method, valid_params) }
				.to raise_error(VtApi::UndefinedMethodError)
			expect { provider.request(invalid_method, {}) }
				.to raise_error(VtApi::UndefinedMethodError)
		end
	end
end
