# frozen_string_literal: true

require 'sinatra/base'
require 'json'

APIKEY            = 'test-key'
FINISHED_RESOURCE = 'test-resource-rc1'
PENDING_RESOURCE  = 'test-resource-rc-2'

FINISHED_RESPONSE = {
	'response_code': 1,
	'verbose_msg':   'Scan finished, scan information embedded in this object',
	'resource':      'test-resource-rc1',
	'scan_id':       'test-resource-r1-0000000001',
	'md5':           '99017f6eebbac24f351415dd410d522d',
	'sha1':          '4d1740485713a2ab3a4f5822a01f645fe8387f92',
	'sha256':        'test-resource-r1',
	'scan_date':     '2010-05-15 03:38:44',
	'permalink':     'https://www.example.com',
	'positives':     1,
	'total':         1,
	'scans':         {
		'SampleAv': {
			'detected': true,
			'version':  '2010-05-14.01',
			'result':   'Trojan.Generic.3611249',
			'update':   '20100514'
		}
	}
}.freeze

PENDING_RESPONSE = {
	'response_code': -2,
	'resource':      'test-resource-rc-2',
	'scan_id':       'test-resource-rc-2',
	'verbose_msg':   'Your resource is queued for analysis'
}.freeze

MISSING_RESPONSE = {
	'response_code': 0,
	'verbose_msg':   'Invalid resource, check what you are submitting'
}.freeze

class FakeApi2 < Sinatra::Base
	get '*/file/report' do
		halt 403 unless request[:apikey] == APIKEY

		case request[:resource]
		when FINISHED_RESOURCE
			FINISHED_RESPONSE
		when PENDING_RESOURCE
			PENDING_RESPONSE
		else
			MISSING_RESPONSE.merge resource: request[:resource]
		end.to_json
	end
end