> __DISCLAIMER__: Tests for this project are not complete yet, coming soon!

# VtApi - VirusTotal API implementation

[![Gem Version](https://badge.fury.io/rb/vt_api.svg)](https://badge.fury.io/rb/vt_api)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/thedeadferryman/vt_api/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/thedeadferryman/vt_api/?branch=master)
[![Build Status](https://scrutinizer-ci.com/b/thedeadferryman/vt_api/badges/build.png?b=master)](https://scrutinizer-ci.com/b/thedeadferryman/vt_api/build-status/master)

VtApi is a Ruby binding to fresh VirusTotal 2.0 API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'vt_api'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install vt_api

## Supported API versions

* [VirusTotal Public API 2.0](https://developers.virustotal.com/v2.0/reference#public-vs-private-api)

More versions (i.e. VT Private API) are on the way.

## Usage

Basic usage:

```ruby
require 'vt_api'

VtApi.token = 'YOUR_TOKEN'

file = VtApi::ApiV2::File.report resource: 'fca0b14b72c426a67f06a771c9e2d67e6883972e' # => #<VtApi::ApiV2::File ...>
scans = file.scans # => [#<VtApi::ApiV2::File::AvResult ...>, ...]  

scan_id = VtApi::ApiV2::File.schedule_scan file: 'foobar.txt' # => "92605f5aa57e20475a893cd63d998ffdafca8b1c6142fca225ea5638a2437fe6-2139190"

url = VtApi::ApiV2::URL.report resource: 'https://bitbucket.org/thedeadferryman/vt_api' # => #<VtApi::ApiV2::URL ...>

comments = VtApi::ApiV2::Comments.get resource: 'fca0b14b72c426a67f06a771c9e2d67e6883972e' # => [#<VtApi::ApiV2::Comments::Comment ...>, ...]
comments = VtApi::ApiV2::Comments.put resource: 'fca0b14b72c426a67f06a771c9e2d67e6883972e', text: 'Lorem ipsum dolor sit amet...' # => true
```
You can change HTTP adapter by using ```VtApi.configure```:

```ruby
VtApi.configure do |opts|
    opts.token = 'YOUR_TOKEN'
    opts.adapter = :net_http # see Faraday gem for more adapters.
end
```

## License

The gem is available as open source under the terms of the [Apache 2.0 Public License](http://www.apache.org/licenses/LICENSE-2.0).
