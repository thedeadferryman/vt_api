# frozen_string_literal: true

require 'date'
require 'pathname'

require 'faraday/upload_io'
require 'mimemagic'

require_relative '../config'
require_relative 'essentials'

module VtApi
	module ApiV2
		# Class that represents VT Public API 2.0 for files.
		class File
			# Representation of particular AV result on file.
			class AvResult
				attr_reader :av_name, :triggered, :version, :threat_name, :update

				# noinspection RubyResolve
				def initialize(av_name, scan_obj)
					@av_name     = av_name
					@triggered   = scan_obj.detected
					@version     = scan_obj.version
					@threat_name = scan_obj.result
					@update      = scan_obj.update
				end
			end

			# Existing ID types
			# @see #id
			ID_TYPES = %w[scan_id resource_id md5 sha1 sha256].freeze

			class << self
				# @see https://developers.virustotal.com/v2.0/reference#file-report
				#
				# @param [String] resource
				# @return [File] File report if present, <code>nil</code> otherwise.
				def report(resource:)
					resp = ApiV2.provider.request 'file.report', apikey: VtApi.options.token, resource: resource
					from_response resp
				end

				# @see https://developers.virustotal.com/v2.0/reference#file-rescan
				#
				# @param [String] resource
				# @return [String] Scan ID.
				def schedule_rescan(resource:)
					resp = ApiV2.provider.request 'file.rescan', apikey: VtApi.options.token, resource: resource
					resp.scan_id
				end

				# @see https://developers.virustotal.com/v2.0/reference#file-scan
				#
				# @param [String|Pathname|UploadIO] file
				# @return [String] Scan ID.
				def schedule_scan(file:)
					file = file_to_io file if file.is_a?(String) || file.is_a?(Pathname)

					resp = ApiV2.provider.request 'file.scan', apikey: VtApi.options.token, file: file
					resp.scan_id
				end

				private

				def file_to_io(file)
					file = ::File.realpath file
					file = ::File.absolute_path file

					mime = MimeMagic.by_path file

					UploadIO.new file, mime
				end
			end

			# Shorthand for #initialize.
			#
			# @see #initialize
			def self.from_response(api_resp)
				# noinspection RubyResolve
				if api_resp.response_code.nil? || (api_resp.response_code < 1)
					nil
				else
					report = new api_resp

					report
				end
			end

			attr_reader :scan_date, :permalink, :scans, :scan_count, :trigger_count

			# Initializes new object from VT API response.
			# @note Direct creation of object cas cause errors since it doesn't contain any validity checks.
			#       Use predefined API method bindings instead.
			#
			# @see .report
			# @see .schedule_scan
			# @see .schedule_rescan
			#
			# @param [OpenStruct] api_resp
			def initialize(api_resp)
				load_ids! api_resp
				load_meta! api_resp
				load_scans! api_resp
			end

			# @return [Fixnum]
			def threat_level
				@trigger_count.to_f / @scan_count
			end

			# Get file identifier.
			# Since VT API offers many ways to identify the file, you can supply ID type to get specific file.
			#
			# @see ID_TYPES
			#
			# @param [Symbol|String] type
			# @return [String] ID string of specified type.
			def id(type = :id)
				raise ArgumentError, "There is no such id type (#{type}) in VT API 2.0" unless ID_TYPES.include? type.to_sym

				@ids[type.to_sym]
			end

			private

			# noinspection RubyResolve
			def load_ids!(api_resp)
				@ids = OpenStruct.new(
					scan_id:     api_resp.scan_id,
					md5:         api_resp.md5,
					sha1:        api_resp.sha1,
					sha256:      api_resp.sha256,
					resource_id: api_resp.resource
				)
			end

			def load_meta!(api_resp)
				@scan_date = DateTime.parse(api_resp.scan_date)
				@permalink = URI(api_resp.permalink)
			end

			# noinspection RubyResolve
			def load_scans!(api_resp)
				@scan_count    = api_resp.total
				@trigger_count = api_resp.positives

				@scans = api_resp.scans.to_h.to_a.map do |key, value|
					[key, AvResult.new(key.to_s, value)]
				end.to_h
			end
		end
	end
end
