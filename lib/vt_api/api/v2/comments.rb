# frozen_string_literal: true


require_relative 'essentials'

module VtApi
	module ApiV2
		# VT API 2.0 for comments
		module Comments
			# Representation of a comment
			class Comment
				attr_reader :text, :date_token, :date

				# Creates new Comment object from API data.
				# It is not recommended to use this directly.
				# Refer to API methods instead.
				#
				# @see Comments.get
				# @see Comments.put
				#
				# @param [String] date_token Date token as present in API (can be used in <code>Comments.get</code>).
				# @param [String] text Comment text
				def initialize(date_token, text)
					@date_token = date_token
					@date       = DateTime.parse date_token
					@text       = text
				end
			end

			# @see https://developers.virustotal.com/v2.0/reference#comments-get
			# @see Comment#date_token
			#
			# @param [String] resource
			# @param [String] before Must be in format of <code>date_token</code>. Refer to API docs for more info.
			# @return [Array<Comment>] Array of parsed comments.
			def self.get(resource:, before:)
				resp = ApiV2.provider.request 'comments.get', apikey: VtApi.options.token, resource: resource, before: before

				parse_comments resp
			end

			# @see https://developers.virustotal.com/v2.0/reference#comments-put
			#
			# @param [String] resource
			# @param [String] text Comment text string.
			# @return [Boolean] True if comment put successfully ('response_code' equals 1), false otherwise.
			def self.put(resource:, text:)
				resp = ApiV2.provider.request 'comments.put', apikey: VtApi.options.token, resource: resource, text: text

				resp.response_code == 1
			end

			def self.parse_comments(api_resp)
				# noinspection RubyResolve
				if api_resp.response_code.nil? || (api_resp.response_code != 1)
					[]
				else
					api_resp.comments.map { |comment| Comment.new comment.date, comment.text }
				end
			end
		end
	end
end
