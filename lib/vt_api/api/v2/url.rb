# frozen_string_literal: true

require_relative '../config'
require_relative 'essentials'

module VtApi
	module ApiV2
		# Class that represents URL scan report.
		class URL
			class << self
				# @see https://developers.virustotal.com/v2.0/reference#url-report
				#
				# @param [String] resource
				# @param [Boolean] scan Schedule URL scan if it is not present in system.
				# @return [URL] URL report object is present, <code>nil</code> otherwise.
				def report(resource:, scan: false)
					resp = ApiV2.provider.request 'url.report',
					                              apikey:        VtApi.options.token,
					                              resource:      resource,
					                              schedule_scan: (scan ? 1 : 0)
					pp resp
					from_response resp
				end

				# @see https://developers.virustotal.com/v2.0/reference#url-scan
				#
				# @param [String] url
				# @return [String] Scheduled scan ID.
				def schedule_scan(url:)
					resp = ApiV2.provider.request 'url.report', apikey: VtApi.options.token, url: url
					resp.scan_id
				end
			end

			# Shorthand for #initialize.
			#
			# @see #initialize
			def self.from_response(api_resp)
				# noinspection RubyResolve
				if api_resp.response_code.nil? || (api_resp.response_code < 1)
					nil
				else
					report = new api_resp
					report
				end
			end

			attr_reader :id, :url, :scan_date, :permalink, :filescan_id

			# Initializes new object from VT API response.
			# @note Direct creation of object cas cause errors since it doesn't contain any validity checks.
			#       Use predefined API method bindings instead.
			#
			# @see .report
			# @see .schedule_scan
			#
			# @param [OpenStruct] api_resp
			def initialize(api_resp)
				load_id!(api_resp)
				load_meta!(api_resp)
				load_scans!(api_resp)
			end

			private

			# noinspection RubyResolve
			def load_scans!(api_resp)
				@scan_count    = api_resp.total
				@trigger_count = api_resp.positives
				@scans         = api_resp.scans
			end

			def load_meta!(api_resp)
				@scan_date   = api_resp.scan_date
				@permalink   = api_resp.permalink
				@filescan_id = api_resp.filescan_id
			end

			# noinspection RubyResolve
			def load_id!(api_resp)
				@id  = api_resp.scan_id
				@url = api_resp.url
			end
		end
	end
end
