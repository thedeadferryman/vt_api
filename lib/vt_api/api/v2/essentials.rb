# frozen_string_literal: true

module VtApi
	# VT Public 2.0 API bindings
	module ApiV2
		# Get ApiProvider singleton configured for VT Public API 2.0.
		#
		# @return [ApiProvider]
		def self.provider
			@provider ||= ApiProvider.new Versions::API_V2, VtApi.options.adapter
		end
	end
end