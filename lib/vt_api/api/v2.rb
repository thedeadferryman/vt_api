# frozen_string_literal: true

require_relative 'config'

require_relative 'v2/essentials'
require_relative 'v2/file'
require_relative 'v2/url'
require_relative 'v2/comments'

module VtApi
	# VT Public 2.0 API bindings
	module ApiV2
	end
end
