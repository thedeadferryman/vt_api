# frozen_string_literal: true

require 'ostruct'

# @author Karl F. Meinkopf
module VtApi
	# Default options for VT API, common for all versions
	DEFAULT_OPTIONS = OpenStruct.new(
		token:   nil,
		adapter: :net_http
	)

	# Get current options
	#
	# @return [OpenStruct]
	def self.options
		@options ||= DEFAULT_OPTIONS
	end

	# Configure VtApi.
	#
	# @yield [opts] Configuration block.
	# @yieldparam opts Options object.
	# @return [Object] Block result.
	def self.configure
		yield options
	end

	# Set VtApi token (apikey).
	#
	# @param [String] token
	# @return [String] Token.
	def self.token=(token)
		options[:token] = token
	end
end
