module VtApi
	# Error is thrown when some of required request params are missing
	#
	# @see Endpoint#params_valid
	# @see ApiProvider#request
	class MissingParametersError < ArgumentError
	end

	# Error is thrown when requested method could not be found in interface.
	#
	# @see ApiVersion#endpoint?
	# @see ApiProvider#request
	class UndefinedMethodError < NoMethodError
	end

	# Error is thrown when API returns HTTP code marked by interface as error.
	#
	# @see ApiVersion#error?
	# @see ApiVersion#error
	# @see ApiProvider#request
	class ApiError < RuntimeError
	end
end