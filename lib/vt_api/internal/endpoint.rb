# frozen_string_literal: true

require 'faraday/connection'

module VtApi
	# API endpoint interface class.
	class Endpoint
		attr_accessor :uri, :method

		# Initialize new endpoint.
		#
		# Interface parameters are described as in example below.
		# @example
		#   {
		#       foo: true, # this describes required parameter
		#       bar: false # this describes a parameter that can be omitted
		#   }
		#
		# @param [Object] uri Relative URL of the endpoint.
		# @param [Object] method HTTP method to be used for the endpoint.
		# @param [Object] params Endpoint parameters interface.
		def initialize(uri, method, params = {})
			@uri    = uri
			@method = method
			@params = params
		end

		# Check whether given parameters match endpoint interface.
		#
		# @param [Hash] passed_params
		# @return [Boolean]
		def params_valid?(passed_params)
			@params.each do |param, required|
				return false if required && passed_params[param].nil?
			end

			true
		end

		# Get a list of missing parameters.
		#
		# @param [Hash] passed_params
		# @return [Array]
		def missing_params(passed_params)
			missing = []

			@params.each do |param, required|
				missing << param if required && passed_params[param].nil?
			end

			missing
		end

		private

		# Remove redundant slashes from URL.
		# @example
		#   clean_url 'http://google.com////search' # => 'http://google.com/search'
		#
		# @param [String] uri
		# @return [String]
		def clean_uri(uri)
			uri.gsub %r{([^:])[\/]+}, '\1/'
		end
	end
end
