# frozen_string_literal: true

module VtApi
	# @abstract Base class for API interfaces.
	class ApiVersion
		# Get API base URL.
		#
		# @return [String]
		def base_url
			''
		end

		# API interface name/version.
		#
		# @return [String]
		def version
			nil
		end

		# Get API HTTP-code description.
		#
		# @param [Integer] _http_code
		# @return [String] Error description.
		def error(_http_code)
			nil
		end

		# Check whether API HTTP-code means error.
		#
		# @param [Integer] _http_code
		# @return [Boolean] Error description.
		def error?(_http_code)
			false
		end

		# Get API method endpoint interface.
		#
		# @param [String] _method API method name to be called.
		# @return [VtApi::Endpoint]
		def endpoint(_method)
			nil
		end

		# Check whether given method is defined in interface.
		#
		# @param [String] _method Method name.
		# @return [Boolean]
		def endpoint?(_method)
			false
		end
	end
end
