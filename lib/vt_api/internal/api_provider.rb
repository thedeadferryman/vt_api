# frozen_string_literal: true

require 'faraday'

require 'json'
require 'ostruct'

require_relative 'versions/default'
require_relative '../errors'

module VtApi
	# API interaction class.
	#
	# Uses [Faraday] for HTTP interaction.
	class ApiProvider
		# Create new ApiProvider.
		#
		# @param [ApiVersion] api_version API interface to use.
		# @param [Object] adapter Faraday HTTP adapter.
		# @return [Object]
		def initialize(api_version = Versions::DEFAULT, adapter = :net_http)
			unless api_version.is_a? ApiVersion
				raise ArgumentError,
				      "Invalid API interface supplied! Must be subclass of 'ApiVersion', got #{api_version.class} instead"
			end

			@api        = api_version
			@connection = Faraday.new @api.base_url do |conn|
				conn.request :multipart
				conn.request :url_encoded

				conn.adapter adapter
			end
		end

		# Performs a request to given API method.
		# Requested method must be described in API interface.
		#
		# @raise [UndefinedMethodError]
		# @raise [MissingParametersError]
		# @raise [ApiError]
		#
		# @param [String] method_name
		# @param [Hash] params
		# @return [OpenStruct] Method result object.
		def request(method_name, params = {})
			endpoint = endpoint(method_name, params)

			result = @connection.public_send(endpoint.method, endpoint.uri, params, {})

			raise ApiError, "#{@api.version} error: #{@api.error(result.status)}" if @api.error? result.status

			# noinspection RubyResolve
			JSON.parse result.body, object_class: OpenStruct
		end

		private

		def endpoint(method_name, params)
			unless @api.endpoint? method_name
				raise UndefinedMethodError,
				      "Endpoint '#{method_name}' not found in '#{@api.version}' API interface."
			end

			endpoint = @api.endpoint method_name

			unless endpoint.params_valid? params
				raise MissingParametersError,
				      "Missed parameters #{endpoint.missing_params(params)} required by endpoint '#{method_name}'."
			end
			endpoint
		end
	end
end
