# frozen_string_literal: true

require_relative '../api_version'
require_relative '../endpoint'

module VtApi
	module Versions
		# VirusTotal Public API 2.0 singleton interface.
		# Describes all available API methods (endpoints).
		class ApiV2 < ApiVersion
			# Root URI for all endpoints.
			BASE_URI = 'https://virustotal.com/vtapi/v2/'

			# VirusTotal Public API v2.0 interface description.
			ENDPOINTS = {
				'file.report':       Endpoint.new('file/report', :get,
				                                  apikey:   true,
				                                  resource: true),
				'file.scan':         Endpoint.new('file/scan', :post,
				                                  apikey: true,
				                                  file:   true),
				'file.rescan':       Endpoint.new('file/rescan', :post,
				                                  apikey:   true,
				                                  resource: true),
				'url.report':        Endpoint.new('url/report', :get,
				                                  apikey:        true,
				                                  resource:      true,
				                                  schedule_scan: false),
				'url.scan':          Endpoint.new('url/scan', :post,
				                                  apikey: true,
				                                  url:    true),
				'domain.report':     Endpoint.new('domain/report', :get,
				                                  apikey: true,
				                                  domain: true),
				'ip-address.report': Endpoint.new('ip-address/report', :get,
				                                  apikey: true,
				                                  domain: true),
				'comments.get':      Endpoint.new('comments/get', :get,
				                                  apikey:   true,
				                                  resource: true,
				                                  before:   false),
				'comments.put':      Endpoint.new('comments/put', :post,
				                                  apikey:   true,
				                                  resource: true,
				                                  comment:  true)
			}.freeze

			# List of possible HTTP error codes with descriptions
			ERRORS = {
				204 => 'Request rate limit exceeded.',
				400 => 'Invalid arguments.',
				403 => 'Access denied.',
				404 => 'Endpoint not found.'
			}.freeze

			class << self
				# Get interface instance.
				#
				# @return [VtApi::Versions::ApiV2]
				def instance
					@instance ||= new
				end
			end

			# Get API method endpoint interface.
			#
			# @param [String] method API method name to be called.
			# @return [VtApi::Endpoint]
			def endpoint(method)
				ENDPOINTS[method.to_sym] unless ENDPOINTS[method.to_sym].nil?
			end

			# Check whether given method is defined in interface.
			#
			# @param [String] method Method name.
			# @return [Boolean]
			def endpoint?(method)
				!endpoint(method).nil?
			end

			# Get API HTTP-code description.
			#
			# @param [Integer] http_code
			# @return [String] Error description.
			def error(http_code)
				ERRORS[http_code] unless ERRORS[http_code].nil?
			end

			# Check whether API HTTP-code means error.
			#
			# @param [Integer] http_code
			# @return [Boolean] Error description.
			def error?(http_code)
				!error(http_code).nil?
			end

			# API base URL.
			#
			# @return [String]
			def base_url
				BASE_URI
			end

			# API interface name/version.
			#
			# @return [String]
			def version
				'VTAPI-2.0'
			end
		end

		# Shorthand for <code>ApiV2.instance</code>
		API_V2 = ApiV2.instance
	end
end
