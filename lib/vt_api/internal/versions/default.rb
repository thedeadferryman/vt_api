# frozen_string_literal: true

require_relative 'api_v2'

module VtApi
	module Versions
		# Default API version used by ApiProvider
		#
		# @see VtApi::ApiProvider
		DEFAULT = API_V2
	end
end
